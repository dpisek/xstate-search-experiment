import { Machine, assign, interpret } from "xstate";

export const searchMachine = Machine(
  {
    id: "search",
    initial: "idle",
    context: {
      results: [],
      query: ""
    },
    on: {
      SEARCH: [
        {
          target: "fetching",
          cond: "isValidSearchQuery"
        },
        {
          target: "idle.withValue",
          cond: "hasQuery"
        },
        { target: "idle" }
      ],
      RESET: {
        target: "idle",
        actions: "clearContext"
      }
    },
    states: {
      idle: {
        initial: "empty",
        states: {
          empty: {},
          withValue: {}
        }
      },
      fetching: {
        invoke: {
          id: "getSearchResults",
          src: "fetchSearchResults",
          onDone: {
            actions: assign((_, { data }) => ({
              results: data
            })),
            target: "success"
          },
          onError: {
            target: "failure"
          }
        }
      },
      success: {
        initial: "unknown",
        states: {
          unknown: {
            on: {
              "": [
                { target: "withData", cond: "resultsNotEmpty" },
                { target: "withoutData" }
              ]
            }
          },
          withData: {},
          withoutData: {}
        }
      },
      failure: {}
    }
  },
  {
    actions: {
      cacheSearchQuery: assign((_, { query }) => ({
        query
      })),
      clearContext: assign(() => ({
        // query: "",
        results: []
      }))
    },
    guards: {
      isValidSearchQuery: (_, { query = "" }) => {
        return query.length >= 3;
      },
      hasQuery: (_, { query = "" }) => {
        return query.length > 0;
      },
      resultsNotEmpty: ({ results }) => results.length > 0
    }
  }
);

export const createSearchService = (fetcher = () => {}) =>
  interpret(
    searchMachine.withConfig({
      services: {
        fetchSearchResults: (_, { query }) => fetcher(query)
      }
    })
  );
